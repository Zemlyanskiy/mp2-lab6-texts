﻿# Лабораторная работа №6: Тексты

## Цели и задачи

В рамках лабораторной работы ставится задача разработки учебного редактора текстов с поддержкой следующих операций:
* выбор текста для редактирования (или создание нового текста);
* демонстрация текста на экране дисплея;
* поддержка средств указания элементов (уровней) текста;
* вставка, удаление и замена строк текста;
* запись подготовленного текста в файл.

### Условия и ограничения
При выполнении лабораторной работы использовались следующие основные предположения:
1. При планировании структуры текста в качестве самого нижнего уровня можно рассматривать уровень строк.
2. В качестве тестовых текстов можно рассматривать текстовые файлы программы.

### План работы
1. Разработка структуры хранения текстов.
2. Разработка методов для доступа к строкам текста.
3. Возможность модификации структуры текста.
4. Создание механизма для обхода текста.
5. Реализация итератора.
6. Копирование текста.
7. Реализация сборки мусора.
8. Тестирование.


### Используемые инструменты
- Система контроля версий **Git**.
- Фреймворк для написания автоматических тестов **Google Test**.
- Среда разработки **Microsoft Visual Studio 2015 Community Edition**.


## Выполнение работы

### 1. TTextLink

TTextLink.h:

    #pragma once

	#include "tdatvalue.h"
	#include <iostream>

	using namespace std;

	const int TextLineLength = 40;

	class TText;
	class TTextLink;

	typedef TTextLink *PTTextLink;
	typedef char TStr[TextLineLength];


	class TTextMem {
		PTTextLink pFirst;     // указатель на первое звено
		PTTextLink pLast;      // указатель на последнее звено
		PTTextLink pFree;      // указатель на первое свободное звено
		TTextMem() : pFirst(NULL), pLast(NULL), pFree(NULL) {};
		friend class TTextLink;
	};
	typedef TTextMem *PTTextMem;

	class TTextLink : public TDatValue {
	protected:
		TStr Str;  // поле для хранения строки текста
		PTTextLink pNext, pDown;  // указатели по тек. уровень и на подуровень
		static TTextMem MemHeader; // система управления памятью
	public:
		static void InitMemSystem(int size); // инициализация памяти
		static void PrintFreeLink(void);  // печать свободных звеньев
		void * operator new (size_t size); // выделение звена
		void operator delete (void *pM);   // освобождение звена
		static void MemCleaner(TText &txt); // сборка мусора
		TTextLink(TStr s = NULL, PTTextLink pn = NULL, PTTextLink pd = NULL) {
			pNext = pn; pDown = pd;
			if (s != NULL) strcpy_s(Str, s); 
			else Str[0] = '\0';
		}
		TTextLink(string s)
		{
			pNext = nullptr; pDown = nullptr;
			strcpy_s(Str, s.c_str());
		}
		~TTextLink() {}
		int IsAtom() { return pDown == NULL; } // проверка атомарности звена
		PTTextLink GetNext() { return pNext; }
		PTTextLink GetDown() { return pDown; }
		PTDatValue GetCopy() { return new TTextLink(Str, pNext, pDown); }
	protected:
		virtual void Print(ostream &os) { os << Str; }
		friend class TText;
	};


TTextLink.cpp:

	#include "TTextLink.h"
	#include "TText.h"

	TTextMem TTextLink::MemHeader;

	void TTextLink::InitMemSystem(int size)
	{
		char *tmp = new char[sizeof(TTextLink)*size];
		MemHeader.pFirst = (PTTextLink)tmp;
		MemHeader.pFree = (PTTextLink)tmp;
		MemHeader.pLast = (PTTextLink)tmp + size - 1;
		PTTextLink pLink = MemHeader.pFirst;

		for (int i = 0; i < size - 1; i++, pLink++)
			pLink->pNext = pLink + 1;
		pLink->pNext = NULL;
	}

	void TTextLink::PrintFreeLink(void)
	{
		PTTextLink pLink = MemHeader.pFree;
		while (pLink != NULL)
		{
			cout << pLink->Str << endl;
			pLink = pLink->pNext;
		}
	}

	void * TTextLink::operator new(size_t size)
	{
		PTTextLink pLink = MemHeader.pFree;
		if (MemHeader.pFree != NULL)  MemHeader.pFree = pLink->pNext;
		return pLink;
	}

	void TTextLink::operator delete(void * pM)
	{
		PTTextLink pLink = (PTTextLink)pM;
		pLink->pNext = MemHeader.pFree;
		MemHeader.pFree = pLink;
	}

	void TTextLink::MemCleaner(TText & txt)
	{
		for (txt.Reset(); !txt.IsTextEnded(); txt.GoNext())
			txt.SetLine("&&&" + txt.GetLine());

		PTTextLink pLink = MemHeader.pFree;
		for (; pLink != nullptr; pLink = pLink->pNext)
			strcpy_s(pLink->Str, "&&&");

		for (pLink = MemHeader.pFirst; pLink <= MemHeader.pLast; pLink++)
			if (strstr(pLink->Str, "&&&") != NULL)
				strcpy_s(pLink->Str, pLink->Str + 3);
			else
				delete pLink;
	}

	
### 2. TText

TText.h:

	#pragma once

	#include "tdatacom.h"
	#include "TTextLink.h"
	#include <stack>
	#include <string>
	#include <fstream>

	class TText;
	typedef TText *PTText;

	class TText : public TDataCom {
	protected:
		PTTextLink pFirst;      // указатель корня дерева
		PTTextLink pCurrent;      // указатель текущей строки
		stack< PTTextLink > Path; // стек траектории движения по тексту
		stack< PTTextLink > St;   // стек для итератора
		PTTextLink GetFirstAtom(PTTextLink pl); // поиск первого атома
		void PrintText(PTTextLink ptl);         // печать текста со звена ptl
		void PrintTextFile(PTTextLink ptl, ofstream & TxtFile);		// печать текста в файл
		PTTextLink ReadText(ifstream &TxtFile); //чтение текста из файла
		static int TextLevel;
	public:
		TText(PTTextLink pl = NULL);
		~TText() { pFirst = NULL; }
		PTText GetCopy();
		// навигация
		Data GoFirstLink(void); // переход к первой строке
		Data GoDownLink(void);  // переход к следующей строке по Down
		Data GoNextLink(void);  // переход к следующей строке по Next
		Data GoPrevLink(void);  // переход к предыдущей позиции в тексте
		// доступ
		string GetLine(void);   // чтение текущей строки
		void SetLine(string s); // замена текущей строки 
		// модификация
		void InsDownLine(string s);    // вставка строки в подуровень
		void InsDownSection(string s); // вставка раздела в подуровень
		void InsNextLine(string s);    // вставка строки в том же уровне
		void InsNextSection(string s); // вставка раздела в том же уровне
		void DelDownLine(void);        // удаление строки в подуровне
		void DelDownSection(void);     // удаление раздела в подуровне
		void DelNextLine(void);        // удаление строки в том же уровне
		void DelNextSection(void);     // удаление раздела в том же уровне
		// итератор
		int Reset(void);              // установить на первую звапись
		int IsTextEnded(void) const;  // текст завершен?
		int GoNext(void);             // переход к следующей записи
		//работа с файлами
		void Read(char * pFileName);  // ввод текста из файла
		void Write(char * pFileName); // вывод текста в файл
		//печать
		void Print(void);             // печать текста
	};

TText.cpp:

	#include "TText.h"

	#define BufLength 80
	static char StrBuf[BufLength + 1]; // буфер дл¤ строк

	int TText::TextLevel;

	TText::TText(PTTextLink pl)
	{
		if (pl == NULL) pl = new TTextLink();
		pFirst = pl;
	}

	// навигаци¤
	Data TText::GoFirstLink(void)
	{
		while (!Path.empty())
			Path.pop();
		pCurrent = pFirst;
		if (pCurrent == NULL)
			SetRetCode(Data::TextError);
		else
			SetRetCode(Data::TextOk);
		return RetCode;
	}


	Data TText::GoDownLink(void)
	{
		if (pCurrent != NULL && pCurrent->GetDown() != NULL) {
			Path.push(pCurrent);
			pCurrent = pCurrent->GetDown();
			SetRetCode(Data::TextOk);
		}
		else
			SetRetCode(Data::TextError);
		return RetCode;
	}

	Data TText::GoNextLink(void)
	{
		if (pCurrent->GetNext() == NULL)
			SetRetCode(Data::TextNoNext);
		else {
			Path.push(pCurrent);
			pCurrent = pCurrent->GetNext();
			SetRetCode(Data::TextOk);
		}
		return RetCode;
	}

	Data TText::GoPrevLink(void)
	{
		if (Path.empty()) SetRetCode(Data::TextNoPrev);
		else {
			pCurrent = Path.top();
			Path.pop();
			SetRetCode(Data::TextOk);
		}
		return RetCode;
	}

	// доступ
	string TText::GetLine(void)
	{
		return string(pCurrent->Str);
	}
	void TText::SetLine(string s)
	{
		strcpy_s(pCurrent->Str, s.c_str());
	}

	// модификаци¤
	void TText::InsDownLine(string s)
	{
		if (pCurrent == NULL) SetRetCode(Data::TextError);
		else {
			TStr buf;
			strcpy_s(buf, s.c_str());
			pCurrent->pDown = new TTextLink(buf, pCurrent->pDown, NULL);
		}
	}

	void TText::InsDownSection(string s)
	{
		if (pCurrent == NULL) SetRetCode(Data::TextError);
		else {
			TStr buf;
			strcpy_s(buf, s.c_str());
			pCurrent->pDown = new TTextLink(buf, NULL, pCurrent->pDown);
		}
	}


	void TText::InsNextLine(string s)
	{
		if (pCurrent == NULL) SetRetCode(Data::TextError);
		else {
			TStr buf;
			strcpy_s(buf, s.c_str());
			pCurrent->pNext = new TTextLink(buf, pCurrent->pNext, NULL);
		}
	}

	void TText::InsNextSection(string s)
	{
		if (pCurrent == NULL) SetRetCode(Data::TextError);
		else {
			TStr buf;
			strcpy_s(buf, s.c_str());
			pCurrent->pNext = new TTextLink(buf, NULL, pCurrent->pNext);
		}
	}

	void TText::DelDownLine(void)
	{
		if (pCurrent == NULL)
			SetRetCode(Data::TextError);
		else if (pCurrent->pDown == NULL)
			SetRetCode(Data::TextNoDown);
		else if (pCurrent->pDown->IsAtom())
			pCurrent->pDown = pCurrent->pDown->pNext;
	}

	void TText::DelDownSection(void)
	{
		if (pCurrent == NULL)
			SetRetCode(Data::TextError);
		else if (pCurrent->pDown == NULL)
			SetRetCode(Data::TextNoDown);
		else
			pCurrent->pDown = pCurrent->pDown->pNext;
	}

	void TText::DelNextLine(void)
	{
		if (pCurrent == NULL)
			SetRetCode(Data::TextError);
		else if (pCurrent->pNext == NULL)
			SetRetCode(Data::TextNoNext);
		else if (pCurrent->pNext->IsAtom())
			pCurrent->pNext = pCurrent->pNext->pNext;
	}

	void TText::DelNextSection(void)
	{
		if (pCurrent == NULL)
			SetRetCode(Data::TextError);
		else if (pCurrent->pNext == NULL)
			SetRetCode(Data::TextNoNext);
		else
			pCurrent->pNext = pCurrent->pNext->pNext;
	}

	// итератор
	int TText::Reset(void)
	{
		while (!St.empty()) St.pop();
		pCurrent = pFirst;
		if (pCurrent != NULL) {
			St.push(pCurrent);
			if (pCurrent->pNext != NULL) St.push(pCurrent->pNext);
			if (pCurrent->pDown != NULL) St.push(pCurrent->pDown);
		}
		return IsTextEnded();
	}

	int TText::IsTextEnded(void) const
	{
		return St.empty();
	}

	int TText::GoNext(void)
	{
		if (!IsTextEnded()) {
			pCurrent = St.top();
			St.pop();
			if (pCurrent != pFirst) {
				if (pCurrent->pNext != NULL) St.push(pCurrent->pNext);
				if (pCurrent->pDown != NULL) St.push(pCurrent->pDown);
			}
		}
		return IsTextEnded();
	}

	PTTextLink TText::GetFirstAtom(PTTextLink pl) // поиск первого атома
	{
		PTTextLink tmp = pl;
		while (!tmp->IsAtom()) {
			St.push(tmp);
			tmp = tmp->GetDown();
		}
		return tmp;
	}


	PTText TText::GetCopy()
	{
		PTTextLink pl1, pl2 = NULL, pl = pFirst, cpl = NULL;

		if (pFirst != NULL) {
			while (!St.empty()) St.pop();
			while (true)
			{
				if (pl != NULL) {									// переход к первому атому
					pl = GetFirstAtom(pl);
					St.push(pl);
					pl = pl->GetDown();
				}
				else if (St.empty()) break;
				else {
					pl1 = St.top(); St.pop();
					if (strstr(pl1->Str, "Copy") == NULL) {			// первый этап, создание копии pDown на уже скопированный подуровень
						pl2 = new TTextLink("Copy", pl1, cpl);
						St.push(pl2);
						pl = pl1->GetNext();
						cpl = NULL;
					}
					else {											// второй этап
						strcpy_s(pl1->Str, pl1->pNext->Str);
						pl1->pNext = cpl;
						cpl = pl1;
					}
				}
			}
		}

		return new TText(cpl);
	}

	//работа с файлами
	void TText::Read(char * pFileName)  // ввод текста из файла
	{
		ifstream TxtFile(pFileName);
		TextLevel = 0;
		if (TxtFile) pFirst = ReadText(TxtFile);
	}

	void TText::Write(char * pFileName) // вывод текста в файл
	{
		TextLevel = 0;
		ofstream TextFile(pFileName);
		PrintTextFile(pFirst, TextFile);
	}

	//печать
	void TText::Print(void)  // печать текста
	{
		TextLevel = 0;
		PrintText(pFirst);
	}

	void TText::PrintText(PTTextLink ptl)         // печать текста со звена ptl
	{
		if (ptl != NULL) {
			for (int i = 0; i < TextLevel; i++) cout << "  ";
			cout << "  " << ptl->Str << endl;
			TextLevel++; 
			PrintText(ptl->GetDown());
			TextLevel--; 
			PrintText(ptl->GetNext());
		}
	}

	void TText::PrintTextFile(PTTextLink ptl, ofstream & TxtFile)         // печать текста со звена ptl
	{
		if (ptl != NULL) {
			for (int i = 0; i < TextLevel; i++) TxtFile << "  ";
			TxtFile << "  " << ptl->Str << endl;
			TextLevel++;
			PrintTextFile(ptl->GetDown(), TxtFile);
			TextLevel--;
			PrintTextFile(ptl->GetNext(), TxtFile);
		}
	}


	PTTextLink TText::ReadText(ifstream& TxtFile)
	{
		string buf;
		PTTextLink ptl = new TTextLink();
		PTTextLink tmp = ptl;
		while (!TxtFile.eof())
		{
			getline(TxtFile, buf);
			if (buf.front() == '}')
				break;
			else if (buf.front() == '{')
				ptl->pDown = ReadText(TxtFile);
			else
			{
				ptl->pNext = new TTextLink(buf.c_str());
				ptl = ptl->pNext;
			}
		}
		ptl = tmp;
		if (tmp->pDown == nullptr)
		{
			tmp = tmp->pNext;
			delete ptl;
		}
		return tmp;
	}
	
	
### 3. Демонстрационная программа
	
	#include "TText.h"

	int main()
	{
		cout << "Input text: " << endl;
		TTextLink::InitMemSystem(200);
		TText text;
		text.Read("C:\\Users\\Ivan\\Documents\\Labs\\mp2-lab6-texts\\src\\Kochin_Zemlyansky\\files\\input.txt");
		text.Print();

		PTText copy = text.GetCopy();
		
		text.GoFirstLink();                // Session 1
		text.GoDownLink();                 // Session 1.1
		text.GoDownLink();                 // Session 1.1.1
		text.GoDownLink();                 // Session 1.1.1.1

		text.DelNextLine();                // del Session 1.1.1.2

		text.GoFirstLink();                 // Session 1
		text.GoNextLink();                  // Session 2
		text.DelDownSection();              // del Session 2.1, 2.2, 2.2.1, 2.3

		text.InsNextLine("Session 3.1.1");  // new Session 3.1.1
		text.InsNextSection("Session 3");   // new Session 3

		text.GoFirstLink();                  // Session 1
		text.GoNextLink();                   // Session 2
		text.GoNextLink();                   // Session 3
		text.InsDownSection("Session 3.1");  // new Session 3.1

		cout << endl << endl << "New Text: " << endl;
		text.Print();
		text.Write("output.txt");
		cout << endl << "Copied input text: " << endl;
		copy->Print();
	}
	
![](demo.png)


### 4. Проверка работоспособности при помощи Google Test Framework

Данные классы были протестированы с помощью фреймворка **Google Test**.

TText_Test.cpp:

	#include "gtest.h"
	#include "../include/TText.h"
	#include <locale>
	#include <iostream>


	TEST(TText, Can_see_an_error_in_the_absence_of_prev) {
		TTextLink::InitMemSystem(2);
		TText T;

		T.GoFirstLink();
		EXPECT_EQ(T.GoPrevLink(), Data::TextNoPrev);
	}

	TEST(TText, Can_create_and_add_text_test) {
		const string str = "Hello world!";
		TTextLink Link = TTextLink(str);
		PTTextLink pLink = &Link;
		TTextLink::InitMemSystem(20);
		TText T(pLink);
		T.GoFirstLink();

		EXPECT_EQ(T.GetLine(), str);
	}

	TEST(TText, Add_and_godown_test) {
		TTextLink::InitMemSystem(20);
		TText T;
		const string str = "Hello world!";
		T.GoFirstLink();
		T.InsDownLine(str);
		T.GoDownLink();

		EXPECT_EQ(T.GetLine(), str);
	}

	TEST(TText, Can_del_down_line_test) {
		const string str1 = "Hello world!";
		const string str2 = "Hello!";
		const string str3 = "World welcomes you";
		TTextLink::InitMemSystem(20);
		TText T;
		T.GoFirstLink();
		T.InsDownLine(str1);
		T.GoDownLink();
		T.InsDownLine(str2);
		T.InsDownLine(str3);
		T.GoDownLink();
		T.DelDownLine();
		T.GoFirstLink();
		T.GoDownLink();

		EXPECT_EQ(T.GetLine(), str1);
		T.GoDownLink();
		EXPECT_EQ(T.GetLine(), str3);
	}

	TEST(TText, Read_test) {
		string str1 = "Start File";
		string str2 = "Continuation File 1.1";
		string str3 = "Continuation File 1.1.1";
		PTTextLink TLink = new TTextLink();
		TTextLink::InitMemSystem(20);
		TText File(TLink);
		File.Read("C:\\Users\\Ivan\\Documents\\Labs\\mp2-lab6-texts\\src\\Kochin_Zemlyansky\\input_files\\input.txt");
		File.GoFirstLink();

		EXPECT_EQ(str1, File.GetLine());
		File.GoDownLink();
		EXPECT_EQ(str2, File.GetLine());
		File.GoDownLink(); 
		EXPECT_EQ(str3, File.GetLine());
	}

	TEST(TText, Can_Del_Next_Section_test) {
		const string sec1 = "section 1";
		const string sec2 = "section 2";
		const string str1 = "string 1";
		const string str2 = "string 2";
		const string str3 = "string 3";
		TTextLink::InitMemSystem(7);
		TText T;

		T.GoFirstLink();
		T.InsDownLine(sec1);
		T.GoDownLink();
		T.InsNextLine(str2);
		T.InsNextLine(str1);
		T.InsNextSection(sec2);
		T.InsNextLine(str3);

		T.Write("dewv.txt");
		T.GoNextLink();
		T.DelNextSection();

		T.GoFirstLink();
		T.GoDownLink();
		T.GoNextLink();
		EXPECT_EQ(T.GetLine(), str3);
	}
	TEST(TText, Can_bring_a_positive_error_code) {
		const string str = "Hello world!";
		TTextLink::InitMemSystem(2);
		TText T;
		T.GoFirstLink();
		T.InsDownLine(str);

		T.GoFirstLink();
		EXPECT_EQ(T.GoDownLink(), Data::TextOk);
	}

	TEST(TText, Can_see_an_error_in_the_absence_of_next) {
		TTextLink::InitMemSystem(2);
		TText T;

		T.GoFirstLink();
		EXPECT_EQ(T.GoNextLink(), Data::TextNoNext);
	}

**Результат**

![](tests.png)


	
## Выводы

В ходе выполнения данной работы были получены навыки создания структуры данных для хранения текста и организации работы с ним, включая доступ к элементам текста, модицикацию структуры текста, обход текста, копирование текста и т.д. 

Функциональность написанной системы была протестирована при помощи Google Test Framework. Тесты показали, что разработанная программа успешно решает поставленную в начале работы задачу.